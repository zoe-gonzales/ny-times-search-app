
// Click event for search button
$("#button-search").on("click", function(event){
    // Prevents "submit" for doing default action
    event.preventDefault();
    var searchTerm = $("#search-term").val().trim();
    var numRecords = $("#num-records").val();
    // adding 0101 to add month and year - API documentation follows this pattern: YYYYMMDD
    var startYear = $("#start-year").val().trim();
    // adding 0101 to add month and year - API documentation follows this pattern: YYYYMMDD
    var endYear = $("#end-year").val().trim();
    var queryURL = `https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=C4AtjjTaRK4nQoKydGZKZ5O1AAPQcSve&q=${searchTerm}`;
    
    if (parseInt(startYear)) {
        queryURL = `${queryURL}&begin_date=${startYear}0101`;
    }
    
    if (parseInt(endYear)) {
        queryURL = `${queryURL}&end_date=${endYear}1231`;
    }
    
    $.ajax({
        url: queryURL,
        method: "GET"
    }).then(function(data){
        // Loop through the number of requested articles (saved in numRecords) to include only the requested #
        for (var i=0; i < numRecords; i++) {
            // Make elements to hold searchTerm info
            var article = $("<div>");
            // The following conditionals account for when the information below is unavailable within the "data" response
            // Testing for article title
            if (data.response.docs[i].headline.main === null || data.response.docs[i].headline.main === undefined) {
                var title = $("<h5>").text(`Title unavailable`);
            } else {
                var title = $("<h5>").text(data.response.docs[i].headline.main);
            }
            // Testing for author
            if (data.response.docs[i].byline.original === null || data.response.docs[i].byline.original === undefined) {
                var author = $("<p>").text(`Author unavailable`);
            } else {
                var author = $("<p>").text(data.response.docs[i].byline.original);
            }
            // Testing for pub date
            if (data.response.docs[i].pub_date === null || data.response.docs[i].pub_date === undefined) {
                var date = $("<p>").text(`Publication date unavailable`);
            } else {
                var date = $("<p>").text(`Published ${convertPubDate(data.response.docs[i].pub_date)}`);
            }
            // Testing for url
            if (data.response.docs[i].web_url === null || data.response.docs[i].web_url === undefined) {
                var link = $("<p>").text(`Link unavailable`);
            } else {
                var link = $("<a>").attr("href", data.response.docs[i].web_url);
                link.text("Read article");
                link.attr("target", "_blank");
            }
            var line = $("<hr>");
            // Append info to article div
            article.append(title, author, date, link, line);
            // Prepend all to the div with id of top-articles
            $("#top-articles").prepend(article);
        }
    });
});

// Function converts pub_date into a readable format
function convertPubDate(str) {
    var newDate = [];
    for (var i=0; i < 10; i++) {
        newDate.push(str[i]);
    }
    // newDate.reverse().join("");
    return newDate.join("");
}

// Click event for clear search button
$("#clear-search").on("click",function(){
    // Empties out top articles div
    $("#top-articles").empty();
});